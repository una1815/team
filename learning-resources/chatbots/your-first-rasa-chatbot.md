# Your first Rasa Chatbot

This tutorial assumes you know how to use the Terminal to move around on withijn your computer's hard drive. If not, check out this video on [bash]()
You also will need to understand the basics of git, but you only need to be able to run a single git command, `git clone` to retrieve the Rasa source code:

```
git clone git@github.com:rasahq/rasa
cd rasa
```

From here on out you can follow along in this video to create a bot that is a bit more polite than the default Rasa playground bot: [your first rasa chatbot](https://tan.sfo2.digitaloceanspaces.com/videos/howto/howto-build-your-first-rasa-chatbot-hobson-eda-50min-2021-02-15.mp4).

If you are a bit more adventurous and just want the summary. Here you go.

## TLDR;

1. Create and activate a conda environment
2. run `poetry install` within the `rasa/` dir 
3. run `rasa init`
4. copy 4 screens of yaml from [rasa playground](https://rasa.com/docs/rasa/playground) to a single file in your rasa project dir
5. train your bot with `rasa train --data ~/rasaproject/copypasta-from-playground.yml --config ~/rasaproject/config.yml`
6. run `conda shell` to chat with your bot 

## Detailed Instructions

### 1. Create a conda environment

Rasa and tensorf flow seem to be compatible with Python 3.7 and they use poetry to install dependencies, so let's add pythoen 3.7 and poetry to our new conda environment called `rasa`.

```console
$ conda create -n rasaenv 'python>=3.7,<3.8' poetry
```

#### *`conda create -n rasaenv 'python>=3.7,<3.8' poetry`*
```console
Collecting package metadata (current_repodata.json): done
Solving environment: done
...
  zlib               pkgs/main/linux-64::zlib-1.2.11-h7b6447c_3
Preparing transaction: done
Verifying transaction: done
Executing transaction: done
# To activate this environment, use
#     $ conda activate rasaenv
# To deactivate an active environment, use
#     $ conda deactivate
```

And don't forget to activate your new environment (just like it says in the `conda create` output text).
You don't want to install rasa in the wrong environment or you'll have to start over:

```console
$ conda activate rasaenv
```

### 2. Clone the Rasa source code

If you haven't already, clone the rasa source code to your local hard drive in a directory where you store all your source code:

```console
$ cd ~/code
$ git clone git@github.com:rasahq/rasa
```

And don't foget to make sure to change into the `rasa/` directory that was just created.

```console
$ cd ~/code/rasa
```

Confirm you're in a directory with the setup.cfg (or setup.py) file so `poetry` and pip can do their magic.

#### *`ls setup*`*
```console
setup.cfg
```

### 3. Install `rasa`

Now you're ready to run the poetry package manager to install rasa:

```console
$ poetry install
```

This will quickly download and install a bunch of python packages. 
The biggest, most problematic one is tensorflow:

#### *`poetry install`*
```console
Installing dependencies from lock file

Package operations: 206 installs, 6 updates, 1 removal

  • Removing wheel (0.36.2)
  • Installing nr.metaclass (0.0.6)
  • Installing nr.collections (0.0.1)
  • Updating chardet (4.0.0 -> 3.0.4)
  ...
  • Installing towncrier (19.2.0)
  • Installing twilio (6.45.4)
  • Installing webexteamssdk (1.6)

Installing the current project: rasa (2.2.4)
```

The `poetry` package manager is much more colorful, more elegant than pip or conda. 

Also, notice that several *channels* are installed, such as `twilio`. That means you could easily create a prototype SMS bot with your new rasa bot.

### 4. Create a `rasa` project

Rasa has a nice interactive initializer that will create a skeleton conversation design (some yaml files) that you can use to get started.
When asked for a path for your project, keep in mind you can only give it a relative or absolute path that does not include any special symbols or variables like `~` -- at least until [Billy's PR](https://github.com/RasaHQ/rasa/pull/7706) is accepted or [our issue](https://github.com/RasaHQ/rasa/issues/7705) is fixed.

So you can create a directory to hold your rasa chatbot projects (yaml files) and `cd` into it before you run `rasa init` to make it easy to remember where rasa put the skeleton files:

```console
$ mkdir ~/code/rasa-projects
$ cd ~/code/rasa-projects
$ rasa init
```

The `rasa init` command will ask a bunch of questions.
You can accept all the defaults, but you probably want to name your first project something like `your-first-rasa-chatbot` so you can remember that this tutorial is why you created it:

#### *`rasa init`*
```console
Welcome to Rasa! 🤖

To get started quickly, an initial project will be created.
If you need some help, check out the documentation at https://rasa.com/docs/rasa.
Now let's start! 👇🏽

? Please enter a path where the project will be created [default: current directory] your-first-rasa-chatbot
```

You probably want to type `your-first-rasa-chatbot` at that first prompt.

For the rest of the prompts you can accept the default or type `y` or **[ENTER]**:

```console
? Please enter a path where the project will be created [default: current directory] your-first-rasa-chatbot                   
? Path '/home/hobs/code/tangibleai/rasa/your-first-rasa-chatbot' does not exist 🧐. Create path?  Yes 

Created project directory at '/home/hobs/code/tangibleai/rasa/your-first-rasa-chatbot'.
Finished creating project structure.
? Do you want to train an initial model? 💪🏽  Yes                                                                             
Training an initial model...
The configuration for policies and pipeline was chosen automatically. It was written into the config file at '/home/hobs/code/tangibleai/rasa/your-first-rasa-chatbot/config.yml'.
Training NLU model...
...
Core model training completed.
Your Rasa model is trained and saved at '~/code/rasa-chatbots/your-first-rasa-chatbot/models/20210215-182706.tar.gz'.

? Do you want to speak to the trained assistant on the command line? 🤖  (Y/n) No

Ok 👍🏼. If you want to speak to the assistant, run 'rasa shell' at any time inside the project directory.
```

### 5. Rasa playground

Point your browser to the [rasa playground](https://rasa.com/docs/rasa/playground).
Copy each of the 4 files from the rasa documentation playground into a single yaml file in your sublime text editor. 

Click next after each file to move on.

You may need to comment out the parts about forms and the actions that refer to them, like we did [here](your-first-rasa-chatbot-from-playground.yml). 

### 6. "Train" your bot

Run `rasa train` and point it to the files you just created. 
Training a rasa chatbot tells it just to figure out what the different intents are and put them into its natural language understanding (NLU) model.
This helps it understand what your user is saying, even when they use words that aren't the same as what you typed into the `yaml` file.

```console
$ rasa train --data ~/rasa-projects/data/your-first-rasa-chatbot-from-playground.yml --config ~/rasa-projects/config.yml
```

Now you should be able to run `conda shell` and it will run your new bot. It doesn't matter what path you're in, your new bot is the default bot and it will always be the brain that tells rasa what to say.
The `rasa run` command is similar to `rasa init` but it creates a web service to act as the brain. 
That way you can talk to your bot over SMS or whatever other channels you've enabled in the rasa `config.yml` file.
 
## What's next?

Next week you'll learn about conversation design. 
You may even learn how to design your own DSL (domain specific language).
Jose, Maria, and I need help designing a language that's simpler than the Rasa yaml syntax, so that school teachers can use it to create quizzes and puzzles for their students.
