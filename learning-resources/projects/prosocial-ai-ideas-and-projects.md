# Prosocial Machine Learning

## Intern Project Ideas

### Chatbot Project Ideas 
- **Rasa Chatbot end-to-end** - build a RASA chatbot based on your own idea, or based on one of Tangible AI's existing projects:
    - 1: create a simple "hello, world" chatbot on RASA and deploy it to the web 
    - 2: connect the chatbot to channel of your choice (Web interface, Telegram, etc.)
    - 3: learn to use quick replies, RASA forms for deterministic dialog
    - 4: implement FAQ and experiment with RASA's NLU model to see if the results change
    - 5: (variation): build a multi-lingual RASA chatbot - use fasttext embeddings 
- **Syndee new functionality** - add conversations and dialogues into Syndee, our flagship chatbot that helps users cope with impostor syndrome. 
    - 1: get familiar with Landbot platform and add small changes to existing flows
    - 2: learn to use Amplitude for product analytics and add new analytics event to Syndee
    - 3: design a new coaching/therapeutic conversation to help the user cope with Impostor Syndrome. 
    - 4: incorporate the tool inside Syndee 
- **sexual education for teens in Tanzania** - create a prototype of a bot to help Tai Tanzania educate youth in Tanzania on sexual education topics
    - 1: get familiar with the project and the organization
    - 2: design the chatbot concept and structure
    - 3: architect the chatbot tech stack 
    - 4: build a prototype to test with users
    - 5: improve the bot after user feedback 

### NLP Project Ideas
- **BPE analysis**: compaire byte pair encoding to n-gram tokenization and case-folding, etc
    - 1: train conventional LSTM model on some common task like translation w/wo case folding, stop words, NLTK tokenizer
    - 2: plot of accuracy vs training time vs learned parameters speed
    - 3: example tokenizations to discover reasons why one better than another
    - 4: expand vocabulary by training BPE tokenizer on Wikipedia
    - 5: transfer learning to translation problem to compare again
- **single-document question answering** - use qary's qa skill to achieve high accuracy of open question answering on a single document (i.e. wikipedia article) (see demo here: https://qary-single-document-demo.bubbleapps.io/)
    - 1: adjust the qa skill to accept a string to find the answer in
    - 2: create a data testset of questions and answers for 1-2 documents/wikipedia pages
    - 3: benchmark qary's accuracy on the testset and improve it 
- **quantified mind**: mind log, consciousness log, quantified brain (self)
    - 1: django form with automatic time tagging of text entry
    - 2: natural language statement of event, categorical for location, numerical for num witnesses, binary for target vs feature (internal sensation vs external event/treatment)
    - 3: autocoding/autocomplete like in doctors office, coding for common events like: take vitamin, eat oatmeal with nuts and fruit, cranberries, pollen, sneeze, cough, sadness, me time, meditate, yoga, pessimism, anxiety, cognitive distortions, happiness, joy, moment of well-being, insight, idea
    - 4: label text not yet autocoded, by clustering words/cocepts with existing autocodes for info extraction (categoricals, tags, clustering of events)
    - 5: train model
    - 6: create web form in django that accomidates basic fields like context, location, time-since event (recorded after the fact by 10 min, 15 min)
    - 7: machine learn the defaults for an individual when they start typing in the description field, user edits the coding that is disconnected from autocomplete once beyond it in desc
    - 8: svelte app (mobile, reactive)
    - 9: qary skill
    - 10: mobile app for qary skill
- **good listener**: qary skill to record user feedback and provide encouragement
    - 1: classifier to indicate whether feedback is on most recent chat
    - 2: regressor to score the quality/helpfulness/sentiment of feedback +1/+100/:-)
    - 3: followup with "thank you." or "noted" or "I'll try to do better next time"
    - 4: follow up with "would you like me to try again?" retriggering the bot after sending it the feedback    
- **nonprofit sentiment**: Twitter ad scraper targeting nonprofits (hardcoded list of nonprofit account @handles, hashtags, keywords). simple sentiment analysis to determine favorability rating and trends and geography -- [How to scrape tweets from Twitter](https://towardsdatascience.com/how-to-scrape-tweets-from-twitter-59287e20f0f1)

### Machine Learning and Computer Vision Project Ideas 
- **breed classifier**: manually create a classifier of dog breeds using height weight length and other measurements
    - one table (csv and excel tab) for each breed
    - practice statistics (mean, std, histogram) on folds of the data for 3 widely separated breeds
    - single variate classifier for the 4 breeds
    - manual thresholds based on midpoint between means
    - add "other" class and show how this can have implications at puppy kennels, dog breeds, etc
    - add "mix" class and show how this can have implications on dogs (euthenizing impure breeds, pure breds relegated to breeding kennels, inbreeding and genetic abnormalities, human society affected by the kinds of dogs they have as companions and their attitude towards other humans of mixed ancestry)
    - making a mistake about a rotweiler could be fatal to a child buying that breed accidentally (just need to improve accuracy? or reduce false negatives on rotweillerness)

### Software Development Project Ideas 
- **generate a flowchart from multiple-linked list** - to allow our in-house project, ConvoScript, to generate Botpress chatbots from a dialog script, we need to create a visual representation of the dialog based on the structure. The aim of the project is to generate a visual representation of a dialog tree (i.e. locations of the nodes) based on the dialog structure.

### Miscellaneous 
- **tutor bot**: math tutor bot by Billy, but having trouble finding data
- **grade level bot**: grade essays 0-18 based on their grade level, by Josh
- **permit lottery prediction**: predict national park permit lottery wins by date, Winter 2021, by Winston
- **garbage classifier**: computer vision image classifier, object detector or image segmenter to identify categories of recyclable material (paper, glass,...), Winter 2021, by Billy, has a working classifier webapp
- **question classifier**: use squad datset to predict questions vs. non-questions, 90% accuracy on simple 2-gram 1000-word count vectorizer, Winter 2021, by Devi
- **fish classifier**: Identify 1 of 4 species of trout with computer vision object detector on raspberry pi, Winter 2020
- **legalese -> plain english**: translate legal terminology and complex statements to simpler language with a lower reading grade level, e.g. "subpoena" -> "document request"
- **slang -> formal english**: also called style transfer
- **sentence paraphraser**: Reword sentences so they say the same thing but in a different way (synonyms, reordering word). N-gram viewer database from Gutenberg would be good source of 5-grams to use.
- **yodafy**: Translate sentences from English to Yoda apostrophized order (subject-object-verb)
- **dialect/style transfer**: english -> Shakespearean,
- **named entity extraction** and identification from personal tweets (from list of small nonprofits)
- **bot detector**: trained on tweets from known bot and nonbot accounts?
- **sentiment analysis**: self-labeled data from Twitter based on hash tags
- **toxic comment detector**: winter 2021 by Uzi, Kaggle dataset
- **topic analysis**: for each nonprofit brand
- **twitter search** from within qary? e.g. user: "tweets about Red Cross?" qary: "sentiment is 75% positive with a trending topic 'donate blood for fires in California'"
- **deflame**: style transfer using GPT-3 to turn toxic comments into nontoxic, helpful
- [Educational Data Nuggets](http://datanuggets.org/view-in-searchable-table/): beginner DS project ideas with very small data sets

### Past Cohorts Intern Projects
- **nmt**: machine translation for chapter 10 in _NLPIA 2nd Ed_, English -> French/Spanish/Amharic/Chinese, Winter 2021, by Winnie and Hanna
    - 1: improve spanish translation
    - 2: french translation
    - 3: bidirectional translation
    - 4: improved translation using more GRU layers
    - 5: improved translation using transformer + GRU decoder
    - 6: amharic dataset cleaning
    - 7: chinese tokenzer + translator?
    - 8: sections/chapter in nlpia
- **stateful quiz**: adaptive quiz that choses the best question based on previous answers, Winter 2021, by Jose
    - 1: `next` param str naming dest state within each state dict, if no `next` then proceed to next state in list (fall through)
    - 2: if `next` param not found then utter error message and proceed to random state
    - 3: `next` is list of dicts with `state` and `answer` as keys
    - 4: `answer` is list of answers (intent) which funnel to same next state
    - 5: next dict can contain `context` key (dict value) which triggers skill.context.update(context)
    - 6: `context` can contain strings with math expressions evaluated on the values during update, e.g. `{'score': '+=1'}`
    - 7: `context` can contain fstrs with variable substitution, e.g. `{'score': '= {happiness} + 1'}`
    - 8: `context` can contain python function name for function that takes previous_context and next_context as 2 mandatory arguments.
    - 9: `state` can have callable python function that takes context, next_context as arguments
    - 10: `state` can have dict that specifies conditionals on context
    - 11: information extraction from statements with fstring format in human answers
- **automatic mind map**: technical documents, [study aid for Jon](./reports/jon-wordmap.ipynb)
    - 1: play with the SpaCy examples in the documentation
    - 2: use spacy to break a text document into words
    - 3: for each word in the spacy Doc object, retrieve the `.vector` attribute (word embeddings)
    - 4: use PCA on all those word vectors to reduce the dimensionality to 2
    - 5: plot a scatterplot of all those 2D word vectors
    - 6: use plotly to create an interactive scatterplot so you can mouse over each word and display its string
    - 7: use clustering (K-Means) to assign an arbitrary cluster class label to each word
    - 8: plot a scatterplot with a different color for each word based on its cluster/class ID
- **inspiring quote**: qary skill to recommend an inspiring quote, Winter 2021, by Billy
    - 1: random quote suggester in `qary` skill
    - 2: content-based filter for unsupervised recommender
    - 3: collaborative filter for unupservised recommender
    - 4: user feedback mechanism: "Thank you!", "Perfect!", ":-)", ":(", "+1"
    - 5: reinforcement learning or "learning to rank" to improve recommendations
- **rap recommender**: chatbot to recommend a line of text that rhymes with the provide phrase, and maybe matches its rhythm Winter 2021, Uzi
    - 1: echo same sentence with last word replaced with rhyme
    - 2: echo same sentence with some synonym substitution and rhyme
    - 3: echo same sentence with slang substitution and rhyme
    - 4: generate multiple sentences with GPT-2 but filter those without rhyme
    - 5: and ness words to expand vocabulary of phone tokenizer and rhymer with soundex
    - 6: hard code additional destemmers for rhymer and phonifier
    - 7: phone expander/tokenizer for proper nouns or any sequence of characters
    - 8: deep learning RNN to generate phones for english words, including misspellings and slang and proper nouns (CMU phonifier/rhymer as training set)
    - 9: augment training set with proper noun lists and google n-gram viewer database
- **qa**: use BERT to answer questions based on a context paragraph, Summer 2020 by Olesya and Travis
- **summarize long tech documents**: Fall 2020 by Gary
- **elastic search** indexing and tuning for better open domain QA, Summer 2020 by Olesya

## NLP Research Projects
NLP research and tutorials could be incorporated into blog posts and NLPIA 2nd edition.
Anyone who contributes code or content or even ideas to NLPIA 2nd Ed will be listed as a contributing author, at a minimum.

- [ ] in ch3 or 4 on sentiment analysis & VADER, reading level estimators in python `textstat` package (Josh project)
- [ ] phone and soundex package that Uzi found
- [ ] rap rhyme project from Uzi
- [ ] linguistics of paraphrasing, moving words around according to their POS and transformations of the syntax tree
- [ ] anastrophe in yoda speak .3% of languages https://www.reddit.com/r/todayilearned/comments/l5unxd/til_yoda_of_the_star_wars_universe_uses_sentences/?utm_medium=android_app&utm_source=share
- [ ] mention semiotics
- [ ] mention linguistics and computational linguistics
- [ ] Mention number naming FSTP 6.3 when talking about named entity extraction
- [ ] mention chatspeak normalization FSTP 6.4 when talking about stemming and lematization

## Advanced ML/DL/AI Project Ideas

These crowdsourced data science platforms are a great source of ideas.
They can give you a head start on a difficult project by providing you with benchmark (state-of-the-art) code and data:

- [kaggle](kaggle.com)
- [papers with code](paperswithcode.com)

## Several projects where the prosoial AI projects could be mentioned

- [San Diego Tech Hub -- AI for Everyone]()
- [Prosocial Machine Learning (ProML)](https://gitlab.com/hobs/proml) for Packt Publishing
    - scheduled to start in March
    - Maria to lead
- [Data Science for Digital Health]() for UCSD
- [Data Science for Good](https://sandiegotechhub.com/data-science/) at San Diego Tech Hub
    - Hobson to copy material for UCSD DSDH course
- [NLPIA 2nd Ed](https://gitlab.com/hobs/nlpia-manuscript) for Manning.com
    - Maria - lead editor
    - Geoff - transformers and attention
    - Winnie (Ch 10-12) and anything RNN or translation
    - Sylvia (Ch 1-4) and anything Chinese
    - Hanna? Amharic? rare language translation
    - Gary - summarization


# 2021-01-14 Blog post GPT-3 and the AI Technological and economic singularity

Background: GPT-3 is 10 percent better at the "Zero-Shot" Penn Treebank word-level language model problem than BERT: https://paperswithcode.com/sota/language-modelling-on-penn-treebank-word

- GPT-3 stats
    - 48 transfomer layers
    - 175B learnable parameters (GPT-2 1.5B, BERT 0.4B)
    - pretrained on data unavailable to anyone but big tech
    - requires massive compute resources just to do inference (to **use** the model)
    - only the architecture is open source and it's not innovative (brute force, add layers)
- GPT-3 performance on benchmark tasks
    - GPT-3 wins on 3 out of 11 general language modeling tasks
    - GPT-2 also wins on 3 out of 11 tasks (and it requires 500x fewer parameters)
    - BERT

[GPT-3 (Generative Pretrained Transformer version 3)](https://en.wikipedia.org/wiki/GPT-3): a recurrent neural network or autoregressive model.

## backlog

[Django](https://www.educative.io/) at educative.io (by Mohammed)

## Ethics

- Hedonism
- Deontology
- Consequentialism
    - Utilitarianism
    - Pragmatics
    - Deweyanism

## Machine Learning

