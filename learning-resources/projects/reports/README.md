# Final Reports

- [Quote Recommender](billy-quote-recommender.md) by Billy Horn (Winter 2021)
- [Word Map](jon-wordmap.ipynb) by Jon Sundin (Winter 2021)
- [Vaccination Uptake](winston-vaccination-report.docx) by Winston Oakley (Winter 2021)
- [Rap Recommender notebook](Rap_recommender.ipynb) and [report](Uzi_final_report) by Uzi Ikwakor (Winter 2021)
- [Neural Machine Translation](https://gitlab.com/tangibleai/team/-/blob/master/learning-resources/projects/reports/hanna-neural-machine-translation.ipynb) by Hanna Seyoum (Winter 2021)

