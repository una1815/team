# Assignment: Fake News, Real NLP
​
Our employer has developed a dataset of News and labeled it as real or fake.
They have asked our Interns to design a model to predict the truthfullness of
an article.

**Create a Jupyter Notebook Report that includes your model and
addresses the following learning goals.**

Questions have been provided to help guide you through the assignment. They are
for your own benefit and you do not need to write out answers for them in your report!
Feel free to pick one (or a few) and share your answers in the Interns slack channel. 
​
### Duration: 7 Days
### Dataset: https://www.kaggle.com/clmentbisaillon/fake-and-real-news-dataset/download
### Objective: Create a hyperparameter table and select the best model.
​
## Learning Goals:
- [ ] Dive into a dataset and review its quality
- [ ] Practice ETL, EDA, Feature Engineering and Model Training.
- [ ] Pick a single model and feauture that you think will be well suited to the problem.
- [ ] Solve an NLP-style problem using a toolset of your selection.
- [ ] Write a few words on how your plan met or did not meet your expectations.
- [ ] Write a abstract and summary for your model explaining why you chose it and its metrics.
​
## Some questions to consider while going through this assignment:
​
### ETL (Extraction, Transform, Load)
- How is our dataset organized and is there any foreseeable issues with it?
- If you were collecting the data would you have organized it differently?
- How was the data generated and do you believe this to be the best way?
- Is there any background on the dataset that could be useful to us?
​
### EDA (Exploratory Data Analysis)
- What are some strengths and weaknesses of your current dataset?
- What features can we create to improve the health of this dataset?
- Are there any features you would want but are unable to create?
- Are a majority of our features one datatype and is this a good or bad thing?
​
### Feature Engineering
- What is your target variable?
- What are the original features of this dataset?
- What are the easiest features for you to expand upon?
- What are the most difficult?
- Value for your time?
​
### Model Training
- Which models are easiest for you to use?
- Which model best fits the features of our expanded dataframe?
- Which set of model best captures the relationship of our target variable with the data?
- Over/Underfitting?
​
### Other
- What are our libraries of choice?
- Do we need to use NLP to solve this problem?
- What is the best method to creating a Hyperparameter table?
- What was the biggest difficulty in this assignment?

If you feel lost feel free to reach out to Dwayne on slack
